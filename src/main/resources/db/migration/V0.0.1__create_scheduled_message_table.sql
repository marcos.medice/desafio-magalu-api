create table scheduled_message (
    id int(11) not null auto_increment,
    external_id varchar(100) not null,
    channel varchar(20) not null,
    addressee varchar(100) not null,
    address varchar(100) not null,
    message text default null,
    scheduled_date timestamp not null,
    status varchar(20) not null,
    creation_date timestamp not null,
    primary key (id),
    KEY `external_id_index` (`external_id`),
    KEY `channel_index` (`channel`),
    KEY `addressee_index` (`addressee`),
    KEY `address_index` (`address`),
    KEY `scheduled_date_index` (`scheduled_date`),
    KEY `status_index` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;