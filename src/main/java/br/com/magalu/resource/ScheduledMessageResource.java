package br.com.magalu.resource;

import br.com.magalu.model.ScheduledMessage;
import br.com.magalu.model.enums.StatusEnum;
import br.com.magalu.resource.model.ScheduledMessagePostRequest;
import br.com.magalu.resource.model.ScheduledMessageResponse;
import br.com.magalu.service.ScheduledMessageService;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class ScheduledMessageResource {

    @Autowired
    private ScheduledMessageService scheduledMessageService;

    @GetMapping(path = "/scheduled-messages", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ScheduledMessageResponse> getScheduledMessage(@RequestParam(value = "externalId", required = true) String externalId) {

        try {

            ScheduledMessage scheduledMessage = this.scheduledMessageService.getByExternalId(externalId);

            if (scheduledMessage == null) {
                return new ResponseEntity(null, HttpStatus.NOT_FOUND);
            }

            ScheduledMessageResponse scheduledMessageResponse = new ScheduledMessageResponse();
            scheduledMessageResponse.setExternalId(scheduledMessage.getExternalId());
            scheduledMessageResponse.setChannel(scheduledMessage.getChannel());
            scheduledMessageResponse.setAddress(scheduledMessage.getAddress());
            scheduledMessageResponse.setAddressee(scheduledMessage.getAddressee());
            scheduledMessageResponse.setMessage(scheduledMessage.getMessage());
            scheduledMessageResponse.setScheduledDate(scheduledMessage.getScheduledDate());
            scheduledMessageResponse.setStatus(scheduledMessage.getStatus());
            scheduledMessageResponse.setCreationDate(scheduledMessage.getCreationDate());

            return new ResponseEntity(scheduledMessageResponse, HttpStatus.OK);

        } catch(ServiceException exception) {
            return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/scheduled-messages")
    @ResponseBody
    public ResponseEntity<ScheduledMessage> postScheduledMessage(@RequestBody ScheduledMessagePostRequest request) {

        try {
            ScheduledMessage scheduledMessage = new ScheduledMessage();

            scheduledMessage.setExternalId(UUID.randomUUID().toString());
            scheduledMessage.setChannel(request.getChannel());
            scheduledMessage.setAddressee(request.getAddressee());
            scheduledMessage.setAddress(request.getAddress());
            scheduledMessage.setMessage(request.getMessage());
            scheduledMessage.setScheduledDate(request.getScheduledDate());
            scheduledMessage.setStatus(StatusEnum.SCHEDULED);

            ScheduledMessage savedScheduledMessage = this.scheduledMessageService.saveScheduledMessage(scheduledMessage);

            return new ResponseEntity(savedScheduledMessage, HttpStatus.CREATED);

        } catch(ServiceException exception) {
            return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(path = "/scheduled-messages")
    @ResponseBody
    public ResponseEntity<ScheduledMessage> postScheduledMessage(@RequestParam(value = "externalId", required = true) String externalId) {

        try {
            ScheduledMessage scheduledMessage = this.scheduledMessageService.getByExternalId(externalId);

            if (scheduledMessage == null) {
                return new ResponseEntity(null, HttpStatus.NOT_FOUND);
            }

            if (scheduledMessage.getStatus() != StatusEnum.SCHEDULED) {
                return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
            }

            scheduledMessage.setStatus(StatusEnum.CANCELLED);

            ScheduledMessage cancelledScheduledMessage = this.scheduledMessageService.saveScheduledMessage(scheduledMessage);

            return new ResponseEntity(cancelledScheduledMessage, HttpStatus.OK);

        } catch(ServiceException exception) {
            return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
