package br.com.magalu.resource.model;

import br.com.magalu.model.enums.ChannelEnum;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

public class ScheduledMessagePostRequest {

    @Enumerated(EnumType.STRING)
    private ChannelEnum channel;
    private String addressee;
    private String address;
    private String message;

    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduledDate;

    public ScheduledMessagePostRequest() {
    }

    public ChannelEnum getChannel() {
        return channel;
    }

    public void setChannel(ChannelEnum channel) {
        this.channel = channel;
    }

    public String getAddressee() {
        return addressee;
    }

    public void setAddressee(String addressee) {
        this.addressee = addressee;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(Date scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    @Override
    public String toString() {
        return "ScheduledMessagePostRequest{" +
                "channel=" + channel +
                ", addressee='" + addressee + '\'' +
                ", address='" + address + '\'' +
                ", message='" + message + '\'' +
                ", scheduledDate=" + scheduledDate +
                '}';
    }
}
