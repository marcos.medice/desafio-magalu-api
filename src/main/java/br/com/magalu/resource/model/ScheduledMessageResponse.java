package br.com.magalu.resource.model;

import br.com.magalu.model.enums.ChannelEnum;
import br.com.magalu.model.enums.StatusEnum;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

public class ScheduledMessageResponse {

    private String externalId;

    @Enumerated(EnumType.STRING)
    private ChannelEnum channel;
    private String addressee;
    private String address;
    private String message;

    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduledDate;

    @Enumerated(EnumType.STRING)
    private StatusEnum status;

    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    public ScheduledMessageResponse() {
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public ChannelEnum getChannel() {
        return channel;
    }

    public void setChannel(ChannelEnum channel) {
        this.channel = channel;
    }

    public String getAddressee() {
        return addressee;
    }

    public void setAddressee(String addressee) {
        this.addressee = addressee;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(Date scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public ScheduledMessageResponse(String externalId, ChannelEnum channel, String addressee, String address, String message, Date scheduledDate, StatusEnum status, Date creationDate) {
        this.externalId = externalId;
        this.channel = channel;
        this.addressee = addressee;
        this.address = address;
        this.message = message;
        this.scheduledDate = scheduledDate;
        this.status = status;
        this.creationDate = creationDate;
    }
}
