package br.com.magalu.repository;

import br.com.magalu.model.ScheduledMessage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScheduledMessageRepository extends JpaRepository<ScheduledMessage, Long> {

    ScheduledMessage findByExternalId(String externalId);
}
