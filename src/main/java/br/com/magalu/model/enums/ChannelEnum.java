package br.com.magalu.model.enums;

public enum ChannelEnum {
    EMAIL,
    SMS,
    PUSH,
    WHATSAPP
}
