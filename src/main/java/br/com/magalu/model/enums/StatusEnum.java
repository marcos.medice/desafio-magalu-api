package br.com.magalu.model.enums;

public enum StatusEnum {
    SCHEDULED,
    SENT,
    CANCELLED
}
