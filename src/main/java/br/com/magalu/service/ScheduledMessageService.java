package br.com.magalu.service;

import br.com.magalu.model.ScheduledMessage;
import br.com.magalu.repository.ScheduledMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScheduledMessageService {

    @Autowired
    private ScheduledMessageRepository scheduledMessageRepository;

    public List<ScheduledMessage> getAll() {
        return this.scheduledMessageRepository.findAll();
    }

    public ScheduledMessage saveScheduledMessage(ScheduledMessage scheduledMessage) {

        if (scheduledMessage == null) {
            return null;
        }

        ScheduledMessage scheduledMessageResponse = this.scheduledMessageRepository.save(scheduledMessage);

        return scheduledMessageResponse;
    }

    public ScheduledMessage getById(Long id) {

        if (id == null) {
            return null;
        }

        return this.scheduledMessageRepository.findById(id).orElse(null);
    }

    public ScheduledMessage getByExternalId(String externalId) {

        if (externalId == null) {
            return null;
        }

        return this.scheduledMessageRepository.findByExternalId(externalId);
    }
}
