package br.com.magalu.resource;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.*;

import br.com.magalu.model.ScheduledMessage;
import br.com.magalu.model.enums.ChannelEnum;
import br.com.magalu.model.enums.StatusEnum;
import br.com.magalu.service.ScheduledMessageService;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

@WebMvcTest
public class ScheduledMessageResourceTest {

    @Autowired
    private ScheduledMessageResource scheduledMessageResource;

    @MockBean
    private ScheduledMessageService scheduledMessageService;

    private MockMvc mockMvc;


    @BeforeEach
    public void setup() {
        standaloneSetup(this.scheduledMessageResource);
    }

    @Test
    public void returnSuccess_WhenGettingScheduledMessage() {

        Long id = Long.parseLong("1");
        Date simulatedDate = new Date();
        ScheduledMessage scheduledMessage = new ScheduledMessage(id, "45f8c469-9040-4d1e-ad97-cb5b6927f903", ChannelEnum.WHATSAPP, "Marcos", "19994921552", "Parabéns", simulatedDate, StatusEnum.CANCELLED, simulatedDate);

        // mockito
        when(this.scheduledMessageService.getByExternalId("45f8c469-9040-4d1e-ad97-cb5b6927f903")).thenReturn(scheduledMessage);

        given()
                .accept(ContentType.JSON)
                .when()
                .get("api/v1/scheduled-messages?externalId=45f8c469-9040-4d1e-ad97-cb5b6927f903")
                .then()
                .statusCode(HttpStatus.OK.value());
    }

    @Test
    public void returnNotFound_WhenGettingScheduledMessage() {

        // mockito
        when(this.scheduledMessageService.getByExternalId("")).thenReturn(null);

        given()
                .accept(ContentType.JSON)
                .when()
                .get("api/v1/scheduled-messages?externalId=")
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }
}
